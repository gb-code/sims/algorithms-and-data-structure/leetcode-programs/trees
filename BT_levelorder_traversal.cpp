/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    vector<vector<int>> levelOrder(TreeNode* root) {
        
         vector<vector<int> > result;
		 
        if(root == NULL)    return result;
        queue<TreeNode *> que[2];
        
		int depth = 0;
        que[depth].push(root);
        
		while(!que[depth].empty())
		{
            vector<int> tmp;
            while(!que[depth].empty())
			{
                TreeNode *node = que[depth].front();
                que[depth].pop();
                tmp.push_back(node->val);
                if(node->left)     que[depth^1].push(node->left);
                if(node->right)    que[depth^1].push(node->right);
            }
            result.push_back(tmp);
            depth ^= 1;
        }
        return result;
    }
};