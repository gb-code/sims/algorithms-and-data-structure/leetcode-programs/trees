/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    vector<int> postorderTraversal(TreeNode* root) {
vector<int> result;
    vector<TreeNode*> start;
    if (root) {
        start.push_back(root);
    }
   
        while (start.size()>0){
        TreeNode *node = start.back();
        start.pop_back();
        result.push_back(node->val);
        if (node->left){
            start.push_back(node->left);
        } 
        if (node->right) {
            start.push_back(node->right);
        }
    }
    std::reverse(result.begin(), result.end());  
    return result;
    }
};