/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    vector<int> preorderTraversal(TreeNode* root) {
        
          vector<int> result;
        stack<TreeNode*> start;
        start.push(root);
        while(start.size()){
            TreeNode *ptr = start.top();
            start.pop();
            while(ptr){
                if(ptr->right)    start.push(ptr->right);
                result.push_back(ptr->val);
                ptr = ptr->left;
            }
        }
        return result;
        
    }
};